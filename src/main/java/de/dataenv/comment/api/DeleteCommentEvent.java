package de.dataenv.comment.api;

public class DeleteCommentEvent implements ICommentEvent {

    private Integer commentId;

    public DeleteCommentEvent() {
    }

    public DeleteCommentEvent(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }
}
