package de.dataenv.comment.api;

public class PostCommentEvent implements ICommentEvent {

    private String text;

    private String userId;

    public PostCommentEvent() {
    }

    public PostCommentEvent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
